import QtQuick 2.9
import Lomiri.Components 1.3


ListItem {
    id: headerListItem

    property alias title: headerText.title

    height: headerText.height

    divider.visible: false

    
    ListItemLayout {
        id: headerText
        title.fontSize: "small"
        title.opacity: 0.75

    }
}