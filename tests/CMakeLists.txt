add_subdirectory(unit)

# Disabling autopilot tests for now due to CI issues

#if(NOT CLICK_MODE)
#  add_subdirectory(autopilot)
#endif(NOT CLICK_MODE)
